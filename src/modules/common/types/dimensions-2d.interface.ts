export interface Dimensions2d {
  height: number;
  width: number;
}
