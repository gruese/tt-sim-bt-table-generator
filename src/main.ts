import '@shoelace-style/shoelace/dist/themes/dark.css';
import './assets/main.css';

import { createApp } from 'vue';
import App from './app.vue';
import router from './router';

// TODO determine if needed when deployed
// import { getBasePath, setBasePath } from '@shoelace-style/shoelace/dist/utilities/base-path';
// setBasePath('/');
// console.log('base path', `"${getBasePath()}"`);

import '@shoelace-style/shoelace/dist/components/alert/alert';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/button-group/button-group';
import '@shoelace-style/shoelace/dist/components/details/details';
import '@shoelace-style/shoelace/dist/components/divider/divider';
import '@shoelace-style/shoelace/dist/components/drawer/drawer';
import '@shoelace-style/shoelace/dist/components/dropdown/dropdown';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/menu/menu';
import '@shoelace-style/shoelace/dist/components/menu-item/menu-item';
import '@shoelace-style/shoelace/dist/components/range/range';
import '@shoelace-style/shoelace/dist/components/tooltip/tooltip';

const app = createApp(App);

app.use(router);

app.mount('#app');
