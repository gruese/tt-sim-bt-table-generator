import { hexCoordinateRound } from '.';
import { SQRT3 } from '@/modules/common/constants';

export function pointToHexCoordinates(x: number, y: number, hexSize = 1) {
  const q = (2 / 3 * x) / hexSize;
  const r = ((-1 / 3 * x) + SQRT3 / 3 * y) / hexSize;
  const s = -q - r;
  return hexCoordinateRound({ q, r, s });
}
