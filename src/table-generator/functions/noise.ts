export function drawNoiseToCanvas(
  ctx: CanvasRenderingContext2D,
  noiseFn: (x: number, y: number) => number,
  width: number,
  height: number,
) {
  const imageData = ctx.createImageData(width, height);
  for (let x = 0; x < width; x++) {
    for (let y = 0; y < height; y++) {
      const idx = 4 * (x + y * width);
      const nx = x / width - 0.5;
      const ny = y / height - 0.5;
      const nVal = (noiseFn(nx, ny) + 1) * 0.5;
      const val = Math.round(nVal * 255);

      imageData.data[idx] = val; // R
      imageData.data[idx + 1] = val; // G
      imageData.data[idx + 2] = val; // B
      imageData.data[idx + 3] = 255; // A
    }
  }

  ctx.putImageData(imageData, 0, 0);
}

export function drawNoiseArrayToCanvas(ctx: CanvasRenderingContext2D, noiseResults: Array<Array<number>>) {
  if (noiseResults.length === 0) {
    return;
  }
  const width = noiseResults.length;
  const height = noiseResults[0].length;
  ctx.clearRect(0, 0, width, height);
  const imageData = ctx.createImageData(noiseResults.length, noiseResults[0].length);
  for (let x = 0; x < noiseResults.length; x++) {
    for (let y = 0; y < noiseResults[0].length; y++) {
      const idx = 4 * (x + y * width);
      // const nx = x / width - 0.5;
      // const ny = y / height - 0.5;
      const val = Math.round(noiseResults[x][y] * 255);
      imageData.data[idx] = val; // R
      imageData.data[idx + 1] = val; // G
      imageData.data[idx + 2] = val; // B
      imageData.data[idx + 3] = 255; // A
    }
  }
  ctx.putImageData(imageData, 0, 0);
}
