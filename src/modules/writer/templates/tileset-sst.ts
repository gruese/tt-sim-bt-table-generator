import { HexFieldType } from '@/modules/common/types';
import type { TerrainMesh } from '@/modules/writer/types';

export const TILESET_SST_MODELS: Record<number, { model: string, collider: string }> = {
  [-3]: {
    model: 'http://cloud-3.steamusercontent.com/ugc/2428074074163129277/F4AA734626FA09E22CFAC5CD6CAFCDDA43B17CEB/',
    collider: 'http://cloud-3.steamusercontent.com/ugc/2428074074163130314/F03B4DEFE908CC6EC22A4EF2DF9B9CF826ED884F/',
  },
  [-2]: {
    model: 'http://cloud-3.steamusercontent.com/ugc/2428074074163126565/F7CBB18774044E584758777D14A6D7227E26386A/',
    collider: 'http://cloud-3.steamusercontent.com/ugc/2428074074163127525/4A930E466329150620B2CB1CA6A4CB5274F2F9D1/',
  },
  [-1]: {
    model: 'http://cloud-3.steamusercontent.com/ugc/2428074074163123286/FD6AED7914DA15C5BEE31068EA1A098FBC116BB1/',
    collider: 'http://cloud-3.steamusercontent.com/ugc/2428074074163124354/662301339BADF0790C66025B15AC87D755C1A78B/',
  },
  0: {
    model: 'http://cloud-3.steamusercontent.com/ugc/2428074074163119910/94CB3B61BCF8C5E1B3897F4C31660CB1881F7540/',
    collider: 'http://cloud-3.steamusercontent.com/ugc/2428074074163121252/B75D4A120F424E39B8F17F30451C8BD4D94DB0E7/',
  },
  1: {
    model: 'http://cloud-3.steamusercontent.com/ugc/2428074074163116274/BB485721692EBC9634498FB43909F833C3A48F81/',
    collider: 'http://cloud-3.steamusercontent.com/ugc/2428074074163117928/2D8423CC5A6AE8AAFEC7004DF27C5403E7834101/',
  },
  2: {
    model: 'http://cloud-3.steamusercontent.com/ugc/2428074074163112418/1C78BBF2AE589B9C738F33A354927B2D21848A0A/',
    collider: 'http://cloud-3.steamusercontent.com/ugc/2428074074163113748/57E9C70C3DF5E4ED0B954221F7C014BAB4B6DCD3/',
  },
  3: {
    model: 'http://cloud-3.steamusercontent.com/ugc/2428074074163106763/75D642B8838C04CD54C7CFEC79E6D407CBBCC3D8/',
    collider: 'http://cloud-3.steamusercontent.com/ugc/2428074074163108224/896E87C8FA551BC6E3A757D87208F97739BC973C/',
  },
  4: {
    model: 'http://cloud-3.steamusercontent.com/ugc/2428074074163102101/46646F1EC7A21841892E82FCE524EAA987111D8E/',
    collider: 'http://cloud-3.steamusercontent.com/ugc/2428074074163104372/C6406D05811C84C9A9ED38127656E1FD51476269/',
  },
  5: {
    model: 'http://cloud-3.steamusercontent.com/ugc/2428074074163073101/FAD07A7C90B3868A3D54404469CFDF83568F9AAF/',
    collider: 'http://cloud-3.steamusercontent.com/ugc/2428074074163077057/C4050FE32FE74DE2E20CF854A3ECAC5E7AB85869/',
  },
  6: {
    model: 'http://cloud-3.steamusercontent.com/ugc/2428074074163082392/F98537FD538C4BF4AF1325D16E12C7157DCCB82C/',
    collider: 'http://cloud-3.steamusercontent.com/ugc/2428074074163100124/5C264DC0BE5B82925D4D54CED451FADCE7353EA5/',
  },
  7: {
    model: 'http://cloud-3.steamusercontent.com/ugc/2428074074163069102/F58B2CA03369D29B76E49757B2C4241FB168C1B9/',
    collider: 'http://cloud-3.steamusercontent.com/ugc/2428074074163070709/42C2C13E76EAB92EDDE4BDAF3FA8F988E4237EF2/',
  },
  8: {
    model: 'http://cloud-3.steamusercontent.com/ugc/2428074074163065318/C25361364FDD0F0A9CE2F91852E803FA6E2F2E88/',
    collider: 'http://cloud-3.steamusercontent.com/ugc/2428074074163066583/D8153D0A6D6F3DE20BFE12549A69543A277DC16A/',
  },
  9: {
    model: 'http://cloud-3.steamusercontent.com/ugc/2428074074163059865/5C6E1ED275E1E9EFDE9161BA4AFAA700BEB0B648/',
    collider: 'http://cloud-3.steamusercontent.com/ugc/2428074074163062028/C993677816DE64822EF6682292195CDA680CC174/',
  },
}

export const TILESET_SST_TEXTURES: Record<HexFieldType, { diffuse?: string, normals?: string }> = {
  [HexFieldType.GRASS]: {
    diffuse: 'http://cloud-3.steamusercontent.com/ugc/2428074074163223808/744D95FC64A8C40E31EC98931780689E2141E21A/',
    normals: 'http://cloud-3.steamusercontent.com/ugc/2428074074163224828/FE9587C9715FA738154D88B7FD1C400BC63D57BA/',
  },
  [HexFieldType.DESERT]: {
    diffuse: 'http://cloud-3.steamusercontent.com/ugc/2428074074163229884/F76F1FE63D636D49388426D8DFD7EFE769FAE836/',
    normals: 'http://cloud-3.steamusercontent.com/ugc/2428074074163232671/3DDBAE37D12CDC06F30AA4600CE737C392DDF8C8/',
  },
  [HexFieldType.MARSH]: {
    diffuse: 'http://cloud-3.steamusercontent.com/ugc/2428074074163245774/D35080BF1786EA2C3A679CB73AC1584A18906296/',
    normals: 'http://cloud-3.steamusercontent.com/ugc/2428074074163248430/14E997EB14F1D821C4D185D65EA5E6BE6420EC86/',
  },
  [HexFieldType.SWAMP]: { // TODO same as marsh
    diffuse: 'http://cloud-3.steamusercontent.com/ugc/2428074074163245774/D35080BF1786EA2C3A679CB73AC1584A18906296/',
    normals: 'http://cloud-3.steamusercontent.com/ugc/2428074074163248430/14E997EB14F1D821C4D185D65EA5E6BE6420EC86/',
  },
  [HexFieldType.WATER]: {
    diffuse: 'http://cloud-3.steamusercontent.com/ugc/2428074074163271362/E70E51B01D80030DFED8A64841CDED9CEEFDCB82/',
    normals: 'http://cloud-3.steamusercontent.com/ugc/2428074074163272591/A0F1FBAA0EEF880584A07F1DAAE672AA579B3843/',
  },
  [HexFieldType.TUNDRA]: {
    diffuse: 'http://cloud-3.steamusercontent.com/ugc/2428074074163253021/0C62DC553BF031AE7AD5162E600B9B22C83F06F6/',
    normals: 'http://cloud-3.steamusercontent.com/ugc/2428074074163254458/9C01F48A1BD935A2313F4E1794CC462DFD53AFB0/',
  },
  [HexFieldType.ASH]: {
    diffuse: 'https://steamusercontent-a.akamaihd.net/ugc/2424718567238951240/70F2FE90DACA0CB3241CF703AB31321853C4EA22/',
    normals: 'http://cloud-3.steamusercontent.com/ugc/2428074074163224828/FE9587C9715FA738154D88B7FD1C400BC63D57BA/',
  },
  [HexFieldType.BOG]: {},
  [HexFieldType.CONCRETE]: {
    diffuse: 'https://steamusercontent-a.akamaihd.net/ugc/42318970627107279/A79A97D940D936587AAC5A8D6B5AD6DD26834D8F/',
    normals: 'https://steamusercontent-a.akamaihd.net/ugc/42318970627107802/4ABBBA4E00EE65CB841CFE6A53C7FA2321E7E540/',
  },
  [HexFieldType.GEYSERS]: {},
  [HexFieldType.ICE]: {
    diffuse: 'https://steamusercontent-a.akamaihd.net/ugc/2381928027749308788/0AEBF3166D13039F55613C82EB58FE6BCDC0CBDB/',
    normals: 'https://steamusercontent-a.akamaihd.net/ugc/2381928027749310112/B3DA1703CE9D447E44E519C7AE8E6A2F66508506/',
  },
  [HexFieldType.ICE_ON_WATER]: { // TODO same as normal ice
    diffuse: 'https://steamusercontent-a.akamaihd.net/ugc/2381928027749308788/0AEBF3166D13039F55613C82EB58FE6BCDC0CBDB/',
    normals: 'https://steamusercontent-a.akamaihd.net/ugc/2381928027749310112/B3DA1703CE9D447E44E519C7AE8E6A2F66508506/',
  },
  [HexFieldType.LAVA]: {},
  [HexFieldType.LUNAR]: {
    diffuse: 'https://steamusercontent-a.akamaihd.net/ugc/42316269060230594/132AA6CD05742628B188883A9B75811331FA5B52/',
    normals: 'https://steamusercontent-a.akamaihd.net/ugc/42316269060231121/AF5A4141748322F0C4D7C7E3283E60777735CFAE/',
  },
  [HexFieldType.SNOW]: {
    diffuse: 'http://cloud-3.steamusercontent.com/ugc/2448350499354203909/347884A82B850D981FE179A9C6244F213B20CE8D/',
    normals: 'http://cloud-3.steamusercontent.com/ugc/2448350499354206615/ECB19F1CD79CB994BC1BD7D8B24510797114AE76/',
  },
  [HexFieldType.WASTELAND]: {
    diffuse: 'https://steamusercontent-a.akamaihd.net/ugc/42316269060241143/0F608D0B4C4FC0817C181365784C382BD30D485E/',
    normals: 'https://steamusercontent-a.akamaihd.net/ugc/42316269060241441/0AFFEE0428DA4A706E260A4463EFDA3FAC783324/',
  },
  [HexFieldType.SAND]: {
    diffuse: 'https://steamusercontent-a.akamaihd.net/ugc/42316269060284651/2F39D937AD51CD49410F57336F0228E620CBE63F/',
    normals: 'https://steamusercontent-a.akamaihd.net/ugc/42316269060285157/E2C92C4073E7BA1E9C0967779EDD53A214AE440F/',
  },
  [HexFieldType.VOLCANIC]: {},
};

export const TILESET_SST_ROTATION_ALLOWED: Record<HexFieldType, boolean> = {
  [HexFieldType.GRASS]: true,
  [HexFieldType.DESERT]: false,
  [HexFieldType.MARSH]: true,
  [HexFieldType.SWAMP]: true,
  [HexFieldType.WATER]: false,
  [HexFieldType.TUNDRA]: true,
  [HexFieldType.ASH]: true,
  [HexFieldType.BOG]: true,
  [HexFieldType.CONCRETE]: true,
  [HexFieldType.GEYSERS]: true,
  [HexFieldType.ICE]: true,
  [HexFieldType.ICE_ON_WATER]: true,
  [HexFieldType.LAVA]: true,
  [HexFieldType.LUNAR]: true,
  [HexFieldType.SNOW]: true,
  [HexFieldType.WASTELAND]: true,
  [HexFieldType.SAND]: false,
  [HexFieldType.VOLCANIC]: true,
};

export const TILESET_SST_WATER_SETTINGS = {
  specularIntensity: 0.05,
  specularSharpness: 3.7,
  fresnelStrength: 0.1,
  castShadows: true,
  colorTint: {
    r: 191,
    g: 208,
    b: 255,
    a: 230,
    hex: '#bgd0ffe6',
  }
}

export function getSstTile(type: HexFieldType, level: number): TerrainMesh {
  const mesh: TerrainMesh = {
    MeshURL: '',
    DiffuseURL: '',
    ColliderURL: '',
  };
  if (TILESET_SST_MODELS[level]) {
    mesh.MeshURL = TILESET_SST_MODELS[level].model || '';
    mesh.ColliderURL = TILESET_SST_MODELS[level].collider || '';
  }
  mesh.DiffuseURL = TILESET_SST_TEXTURES[type].diffuse || '';
  mesh.NormalURL = TILESET_SST_TEXTURES[type].normals || '';

  if (level < 0) {
    mesh.ColorDiffuse = {
      r: 191 / 255,
      g: 208 / 255,
      b: 255 / 255,
      a: 230 / 255,
    };
    mesh.CustomShader = {
      SpecularColor: {
        r: 1,
        g: 1,
        b: 1,
      },
      SpecularIntensity: 0.05,
      SpecularSharpness: 3.75,
      FresnelStrength: 0.1,
    };
  }

  return mesh;
}
