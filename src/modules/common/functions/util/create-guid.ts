/**
 * Randomly generates a random GUID, a string of hexadecimal numbers,
 * e.g. "59df4e"
 *
 * @param length The length of the generated guid
 */
export function createGuid(length = 6) {
  const guid: Array<string> = [];
  for (let i = 0; i < length; i++) {
    guid.push(Math.floor(Math.random() * 16).toString(16));
  }
  return guid.join('');
}
