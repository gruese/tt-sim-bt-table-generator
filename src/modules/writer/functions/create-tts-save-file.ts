import type { SaveGame, TerrainMesh, ObjectState } from '../types';
import { templateTableSimple, TERRAIN_MESHES_DEFAULT, TEMPLATE_OBJECT_STATE, HEX_ADDITIONS_DEFAULT } from '../templates';
import type { HexField } from '@/modules/common/types';
import { createGuid, hexCoordinatesToPoint } from '@/modules/common/functions';
import { ITERATION_LIMIT } from '@/modules/common/constants';
import { getSstTile, TILESET_SST_ROTATION_ALLOWED } from '@/modules/writer/templates/tileset-sst';

const SAVE_FILE = templateTableSimple;
const TERRAIN_MESHES = TERRAIN_MESHES_DEFAULT;
const ADDITION_MESHES = HEX_ADDITIONS_DEFAULT;

// fallback mesh
const DEFAULT_MESH: TerrainMesh = {
  MeshURL: 'http://cloud-3.steamusercontent.com/ugc/907946686119114490/E237D41935846DEE2A51FAA1EEAA83A0F7567DA5/',
  DiffuseURL: 'http://cloud-3.steamusercontent.com/ugc/907946686119117099/0CE8A99F42C47229A0BA2D3AA7A6B7DC3BFECAFB/',
  ColliderURL: 'http://cloud-3.steamusercontent.com/ugc/907946686119114490/E237D41935846DEE2A51FAA1EEAA83A0F7567DA5/',
}

export function createTtsSaveFile(hexFields: Record<string, HexField>, name = 'SST generated map'): {
  saveFile: SaveGame,
  warnings: Array<string>,
} {
  const warnings: Array<string> = [];
  const saveFile = JSON.parse(JSON.stringify(SAVE_FILE)) as SaveGame;
  const guids = saveFile.ObjectStates.map((state) => state.GUID);

  // determine hex size: check existing grid size, or just use 1
  let hexSize = 1;
  if (saveFile.Grid && saveFile.Grid.Type === 1) {
    hexSize = (saveFile.Grid.xSize || 2) / 2;
  }

  const modelScaleFactor = 1.33; // TODO should be configurable per model
  const tableLevel = 1.47; // TODO should be configurable
  const heightPerLevel = 0.6875; // TODO should be configurable
  const baseHeight = 0.1; // height of the lowest collider's floor TODO should be configurable
  const groundLevel = tableLevel + (baseHeight + 3 * heightPerLevel) * modelScaleFactor; // TODO magic numbers
  // const groundLevel = tableLevel + 0.25 * modelScaleFactor; // TODO should be configurable
  const lockTerrainObjects = true;

  // set save file name
  saveFile.SaveName = name;

  // set save file date and time
  const now = new Date();
  saveFile.EpochTime = Math.round(now.getTime() / 1000);
  saveFile.Date = `${now.toLocaleDateString('en-US')} ${now.toLocaleTimeString('en-US')}`;

  Object.values(hexFields).forEach((hexField) => {
    // create unique guid for hex terrain
    const guid = createUniqueGuid(guids);
    // figure out hex's x, y position
    const position = hexCoordinatesToPoint(hexField, hexSize);

    // mesh
    let mesh = getSstTile(hexField.type, hexField.heightLevel);
    // let mesh = TERRAIN_MESHES[hexField.type] ? TERRAIN_MESHES[hexField.type][hexField.heightLevel] : null;
    if (!mesh) {
      warnings.push(`Custom object not found for hex type "${hexField.type}", height level ${hexField.heightLevel}. Using default object (Grass Level 0).`);
      console.error(warnings[warnings.length - 1]);
      mesh = DEFAULT_MESH;
    }

    // create object state entry in savegame
    const objectState: ObjectState = {
      ...TEMPLATE_OBJECT_STATE,
      GUID: guid,
      Nickname: hexField.heightLevel < 0 ? `Depth ${-hexField.heightLevel}`: `Level ${hexField.heightLevel}`,
      Locked: lockTerrainObjects,
      ColorDiffuse: mesh.ColorDiffuse || TEMPLATE_OBJECT_STATE.ColorDiffuse,
      CustomMesh: {
        ...TEMPLATE_OBJECT_STATE.CustomMesh,
        NormalURL: '',
        ...mesh,
      },
      Transform: {
        ...TEMPLATE_OBJECT_STATE.Transform,
        posX: position.x,
        posY: tableLevel,
        posZ: -position.y,
        rotY: TILESET_SST_ROTATION_ALLOWED[hexField.type] ? Math.floor(Math.random() * 6) * 60 : 0,
        scaleX: modelScaleFactor,
        scaleY: modelScaleFactor,
        scaleZ: modelScaleFactor,
      }
    };
    saveFile.ObjectStates.push(objectState);
    guids.push(guid);

    // create addition object (woods, rough grounds etc.)
    const additionGuid = createUniqueGuid(guids);
    // addition mesh
    const additionMesh = hexField.woodsType
      ? ADDITION_MESHES['WOODS__' + hexField.woodsType]
      : hexField.roughType
      ? ADDITION_MESHES['ROUGH__' + hexField.roughType]
      : null;
    // output warnings if no mesh can be found
    if (hexField.woodsType && !additionMesh) {
      warnings.push(`Custom object not found for woods type "${hexField.woodsType}", will be skipped`);
      console.error(warnings[warnings.length - 1]);
    } else if (hexField.roughType && !additionMesh) {
      warnings.push(`Custom object not found for rough grounds type "${hexField.roughType}", will be skipped`);
      console.error(warnings[warnings.length - 1]);
    }
    if (additionMesh) {
      const additionObject: ObjectState = {
        ...TEMPLATE_OBJECT_STATE,
        GUID: additionGuid,
        Locked: lockTerrainObjects,
        ...additionMesh,
        Transform: {
          ...TEMPLATE_OBJECT_STATE.Transform,
          posX: position.x,
          posY: groundLevel + hexField.heightLevel * heightPerLevel * modelScaleFactor,
          posZ: -position.y,
          rotY: Math.floor(Math.random() * 12) * 30,
          scaleX: modelScaleFactor,
          scaleY: modelScaleFactor,
          scaleZ: modelScaleFactor,
        }
      };
      saveFile.ObjectStates.push(additionObject);
      guids.push(additionGuid);
    }
  });

  return {
    saveFile,
    warnings,
  };
}

/**
 * Helper function that attempts to create a unique GUID or outputs an error message.
 *
 * @param guids The array of already existing GUIDs
 */
function createUniqueGuid(guids: Array<string>) {
  let guid = '';
  for (let i = 0; i < ITERATION_LIMIT && (guid === '' || guids.includes(guid)); i++) {
    guid = createGuid();
    if (i >= ITERATION_LIMIT - 1) {
      console.error('Iteration limit violated when trying to generate unique GUID');
    }
  }
  return guid;
}
