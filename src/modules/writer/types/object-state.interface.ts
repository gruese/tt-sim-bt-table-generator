import type { RgbColor, RgbaColor } from '.';

export interface ObjectState {
  GUID: string;
  Name: string;
  Transform: {
    posX: number;
    posY: number;
    posZ: number;
    rotX: number;
    rotY: number;
    rotZ: number;
    scaleX: number;
    scaleY: number;
    scaleZ: number;
  };
  Nickname: string;
  Description: string;
  GMNotes: string;
  AltLookAngle: {
    x: number;
    y: number;
    z: number;
  };
  ColorDiffuse: RgbColor | RgbaColor;
  LayoutGroupSortIndex: number;
  Value: number;
  Locked: boolean;
  Grid: boolean;
  Snap: boolean;
  IgnoreFoW: boolean;
  MeasureMovement: boolean;
  DragSelectable: boolean;
  Autoraise: boolean;
  Sticky: boolean;
  Tooltip: boolean;
  GridProjection: boolean;
  HideWhenFaceDown: boolean;
  Hands: boolean;
  FogColor?: string;
  CustomMesh?: {
    MeshURL: string;
    DiffuseURL: string;
    NormalURL: string;
    ColliderURL: string;
    Convex: boolean;
    MaterialIndex: number;
    TypeIndex: number;
    CustomShader: {
      SpecularColor: RgbColor;
      SpecularIntensity: number;
      SpecularSharpness: number;
      FresnelStrength: number;
    };
    CastShadows: boolean;
  },
  LuaScript: string;
  LuaScriptState: string;
  XmlUI: string;
}
