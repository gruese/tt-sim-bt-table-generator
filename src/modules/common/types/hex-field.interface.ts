import type { HexCoordinates, HexFieldType, RoughnessType, WoodsType } from '../types';

export interface HexField extends HexCoordinates {
  key: string;
  size: number;
  passable: boolean;
  elevation: number;
  heightLevel: number;
  type: HexFieldType;
  humidity: number;
  totalHumidity: number;
  woodiness: number;
  woodsType?: WoodsType;
  roughness: number;
  roughType?: RoughnessType;
}
