import { WoodsType } from '@/modules/common/types';

export function cycleWoodsTypes(currentType: WoodsType | undefined) {
  const types = [... Object.keys(WoodsType), undefined];
  const currentTypeIdx = types.findIndex((type) => type === currentType);
  return types[((currentTypeIdx || 0) + 1) % types.length] as (WoodsType | undefined);
}
