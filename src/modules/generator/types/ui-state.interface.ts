import type { GeneratorStep, MapSizeUnit } from '.';
import type { Dimensions2d } from '@/modules/common/types';

export interface UiState {
  activeStep: GeneratorStep;
  seedInputLocked: boolean;
  showSeedResetWarning: boolean;
  mapSizeUnit: MapSizeUnit;
  mapSizeInput: Dimensions2d;
  settingsPanelVisible: boolean;
}
