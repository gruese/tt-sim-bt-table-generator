/**
 * Use the samples list to determine mean and standard deviation of a normal distribution
 *
 * @param samples List of all sample values
 */
export function determineMeanAndSigma(samples: Array<number>) {
  // calculate mean and standard deviation
  const mean = samples.reduce((sum, curr) => sum + curr, 0) / samples.length;
  const sigma = Math.sqrt(samples.reduce((sum, curr) => sum + (mean - curr) ** 2, 0) / (samples.length - 1));
  return { mean, sigma };
}