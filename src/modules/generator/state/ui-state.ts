import { reactive } from 'vue';
import { GeneratorStep, type UiState, type MapSizeUnit } from '../types';
import { generatorState, updateNoiseFn } from './generator-state';
import { MAP_SIZE_DIMENSIONS } from '@/modules/generator/constants';
import { generateMap } from '@/modules/generator/functions';

export const uiState = reactive<UiState>({
  activeStep: GeneratorStep.SEED,
  seedInputLocked: false,
  showSeedResetWarning: false,
  mapSizeUnit: 'sheets-landscape',
  mapSizeInput: { height: 1 , width: 1},
  settingsPanelVisible: false,
});

export function setMapSizeUnit(mapSizeUnit: MapSizeUnit) {
  if (mapSizeUnit === uiState.mapSizeUnit) {
    return;
  }
  if (mapSizeUnit === 'hexes') {
    uiState.mapSizeInput.width = generatorState.mapSize.width;
    uiState.mapSizeInput.height = generatorState.mapSize.height;
  } else if (uiState.mapSizeUnit === 'hexes') {
    // when switching from hexes to map sheets, round the number of hexes to the closest integer of
    // map sheets in each direction
    const mapSheetHorizontalHexCount = mapSizeUnit === 'sheets-landscape' ? MAP_SIZE_DIMENSIONS.long : MAP_SIZE_DIMENSIONS.short;
    const mapSheetVerticalHexCount = mapSizeUnit === 'sheets-landscape' ? MAP_SIZE_DIMENSIONS.short : MAP_SIZE_DIMENSIONS.long;
    uiState.mapSizeInput.width = Math.max(
      1,
      Math.round(generatorState.mapSize.width / mapSheetHorizontalHexCount),
    );
    uiState.mapSizeInput.height = Math.max(
      1,
      Math.round(generatorState.mapSize.height / mapSheetVerticalHexCount),
    );
    console.log('rounded to', uiState.mapSizeInput.width, uiState.mapSizeInput.height, 'using', mapSheetHorizontalHexCount, mapSheetVerticalHexCount,
      'from', generatorState.mapSize.width, generatorState.mapSize.height);
  }
  uiState.mapSizeUnit = mapSizeUnit;
  // change the map's actual hex size according to the rounding result
  synchronizeMapSize();
}

/**
 * Synchronizes map size between UI state and generatorState
 */
export function synchronizeMapSize() {
  const mapSheetHorizontalHexCount =
    uiState.mapSizeUnit === 'hexes'
      ? 1
      : uiState.mapSizeUnit === 'sheets-landscape'
      ? MAP_SIZE_DIMENSIONS.long
      : MAP_SIZE_DIMENSIONS.short;
  const mapSheetVerticalHexCount =
    uiState.mapSizeUnit === 'hexes'
      ? 1
      : uiState.mapSizeUnit === 'sheets-landscape'
      ? MAP_SIZE_DIMENSIONS.short
      : MAP_SIZE_DIMENSIONS.long;
  generatorState.mapSize.width = uiState.mapSizeInput.width * mapSheetHorizontalHexCount;
  generatorState.mapSize.height = uiState.mapSizeInput.height * mapSheetVerticalHexCount;
}
export function goForwardToNextStep() {
  switch (uiState.activeStep) {
    case GeneratorStep.SEED:
      uiState.activeStep = GeneratorStep.PARAMETERS;
      if (!uiState.seedInputLocked) {
        updateNoiseFn();
        lockSeed();
        generateMap();
      }
      break;
    case GeneratorStep.PARAMETERS:
      uiState.activeStep = GeneratorStep.MANUAL;
      break;
  }
}

export function goBackToPreviousStep() {
  switch (uiState.activeStep) {
    case GeneratorStep.PARAMETERS:
      uiState.activeStep = GeneratorStep.SEED;
      break;
    case GeneratorStep.MANUAL:
      uiState.activeStep = GeneratorStep.PARAMETERS;
      break;
  }
}

export function lockSeed() {
  uiState.seedInputLocked = true;
  setTimeout(() => uiState.showSeedResetWarning = true, 1000);
}

export function unlockSeed() {
  uiState.seedInputLocked = false;
  uiState.showSeedResetWarning = false;
}
