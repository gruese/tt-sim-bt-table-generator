import { createNoise2D, type NoiseFunction2D } from 'simplex-noise';
import alea from 'alea';

export function createFractalNoiseFn(numberOfOctaves = 1, seed = 'sst') {
  const noiseFns: Array<NoiseFunction2D> = [];
  for (let o = 0; o < numberOfOctaves; o++) {
    noiseFns.push(createNoise2D(alea(seed)));
  }
  return (nx: number, ny: number) => {
    let n = 0;
    const weights: Array<number> = [];
    for (let o = 0; o < numberOfOctaves; o++) {
      const frequency = 2 ** o;
      const weight = 1 / frequency;
      n += weight * noiseFns[o](frequency * nx, frequency * ny);
      weights.push(weight);
    }
    return n / weights.reduce((sum, curr) => sum + curr, 0);
  };
}
