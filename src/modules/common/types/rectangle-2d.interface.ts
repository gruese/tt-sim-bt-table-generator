export interface Rectangle2d {
  left: number;
  right: number;
  top: number;
  bottom: number;
}
