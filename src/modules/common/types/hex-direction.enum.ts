export enum HexDirection {
  'UP' = 0,
  'UP_RIGHT' = 1,
  'DOWN_RIGHT' = 2,
  'DOWN' = 3,
  'DOWN_LEFT' = 4,
  'UP_LEFT' = 5,
}
