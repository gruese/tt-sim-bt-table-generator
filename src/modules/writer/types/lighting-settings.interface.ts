import type { RgbColor } from '.'

export interface LightingSettings {
  LightIntensity: number;
  LightColor: RgbColor;
  AmbientIntensity: number;
  AmbientType: number;
  AmbientSkyColor: RgbColor;
  AmbientEquatorColor: RgbColor;
  AmbientGroundColor: RgbColor;
  ReflectionIntensity: number;
  LutIndex: number;
  LutContribution: number;
}