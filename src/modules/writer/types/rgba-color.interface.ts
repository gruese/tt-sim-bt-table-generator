import type { RgbColor } from '.';

export interface RgbaColor extends RgbColor {
  a: number;
}
