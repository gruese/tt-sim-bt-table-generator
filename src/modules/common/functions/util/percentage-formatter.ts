export function percentageFormatter(v: number, precision = 1) {
  return `${(v * 100).toFixed(precision)}%`;
}
