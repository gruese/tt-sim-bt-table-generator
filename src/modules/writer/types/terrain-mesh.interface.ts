import type { RgbColor, RgbaColor } from '.';

export interface TerrainMesh {
  MeshURL: string;
  ColliderURL: string;
  DiffuseURL: string;
  NormalURL?: string;
  ColorDiffuse?: RgbColor | RgbaColor;
  CustomShader?: {
    SpecularColor: RgbColor;
    SpecularIntensity: number;
    SpecularSharpness: number;
    FresnelStrength: number;
  };
}
