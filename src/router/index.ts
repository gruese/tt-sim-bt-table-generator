import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '@/modules/ui/components/home-page.vue';
import Documentation from '@/modules/ui/components/documentation.vue';
import Generator from '@/modules/generator/components/generator.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage,
    },
    {
      path: '/docs',
      name: 'docs',
      component: Documentation,
    },
    {
      path: '/generator',
      name: 'generator',
      component: Generator,
    }
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (About.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import('../views/AboutView.vue'),
    // },
  ],
})

export default router;
