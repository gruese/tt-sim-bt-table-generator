import { HexFieldType } from '@/modules/common/types';

export function cycleHexTypes(currentType: HexFieldType, forward = true) {
  const types = Object.keys(HexFieldType).filter((key) => !['WATER', 'ICE_ON_WATER', 'LAVA'].includes(key));
  if (!forward) {
    types.reverse();
  }
  const currentIdx = types.findIndex((type) => type === currentType);
  const newIndex = ((currentIdx || 0) + 1) % types.length;
  return types[newIndex] as HexFieldType;
}
