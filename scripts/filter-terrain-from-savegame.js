import fs from 'fs';

/**
 * Call like this:
 * > npm run scripts:filter-terrain <mapfile> <setupfile> <outfile>
 */
function filterTerrainFromSaveGame() {
  const savegame = process.argv[2];
  const mergeInto = process.argv[3];
  const outFile = process.argv[4];
  const parsedSavegame = JSON.parse(
    fs.readFileSync(savegame, {
      encoding: 'utf-8'
    }),
  );
  const terrainOnly = parsedSavegame.ObjectStates.filter((objState) =>
    objState.Nickname.startsWith('Level ')
    || objState.Nickname.startsWith('Depth ')
    || objState.Nickname.endsWith('Woods')
    || objState.Nickname === 'Rough'
  );
  const parsedMergeInto = JSON.parse(
    fs.readFileSync(mergeInto, {
      encoding: 'utf-8',
    }),
  );

  // remove terrain from target
  parsedMergeInto.ObjectStates = parsedMergeInto.ObjectStates.filter((objState) => 
    !(
      objState.Nickname.startsWith('Level ') ||
      objState.Nickname.startsWith('Depth ') ||
      objState.Nickname.endsWith('Woods') ||
      objState.Nickname === 'Rough'
    )
  );
  parsedMergeInto.ObjectStates.push(...terrainOnly);

  fs.writeFileSync(outFile, JSON.stringify(parsedMergeInto), {
    encoding: 'utf-8',
  });

  console.log(`merged ${terrainOnly.length} terrain pieces into ${outFile}`);

}

removeTundraTiles() {
  const savegame = process.argv[2];
  const mergeInto = process.argv[3];
  const outFile = process.argv[4];
  const parsedSavegame = JSON.parse(
    fs.readFileSync(savegame, {
      encoding: 'utf-8'
    }),
  );

  const newTerrain = parsedSavegame.ObjectStates.filter((objState) =>
    objState.Nickname === ''
  );
}


filterTerrainFromSaveGame();