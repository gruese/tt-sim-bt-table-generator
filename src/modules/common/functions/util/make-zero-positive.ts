export function makeZeroPositive(num: number) {
  // looks weird, but 0 === -0 is true, so this removes negative zeroes
  return num === 0 ? 0 : num;
}