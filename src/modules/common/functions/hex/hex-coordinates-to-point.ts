import type { HexCoordinates } from '@/modules/common/types';
import { SQRT3 } from '@/modules/common/constants';

export function hexCoordinatesToPoint(hex: HexCoordinates, hexSize = 1) {
  return {
    x: hexSize * (hex.q * 3 / 2),
    y: hexSize * (hex.q * SQRT3 / 2 + hex.r * SQRT3),
  };
}
