/**
 * The cube coordinates for a given hex
 * @see https://www.redblobgames.com/grids/hexagons/#coordinates-cube
 */
export interface HexCoordinates {
  q: number;
  r: number;
  s: number;
}