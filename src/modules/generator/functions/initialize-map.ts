import { generatorState } from '@/modules/generator/state';
import { SQRT3 } from '@/modules/common/constants';
import { HexDirection, HexFieldType } from '@/modules/common/types';
import type { HexCoordinates, HexField, Rectangle2d } from '@/modules/common/types';
import { clampNumber, getHexFieldKey, hexCoordinatesToPoint, hexStep } from '@/modules/common/functions';

/**
 * (Re-)initializes the hex map based on the current parameters.
 * Initialization includes
 * - determining map bounds
 * - creating hex field objects and setting their base values determined by the noise function
 * - determining maximum and minimum elevation values
 */
export function initializeMap() {
  const fields: Record<string, HexField> = {};

  const stepsUp = Math.floor((generatorState.mapSize.height  - 1) *  0.5);
  const stepsLeft = Math.floor((generatorState.mapSize.width - 1) * 0.5);

  const spacing = { h: generatorState.hexSize * 3 / 2, v: generatorState.hexSize * SQRT3 };

  const mapWidth = spacing.h * (generatorState.mapSize.width - 1) + generatorState.hexSize * 2;
  const mapHeight = spacing.v * generatorState.mapSize.height + spacing.v * 0.5;

  // find top left hex
  let topLeftHexCoordinates: HexCoordinates = { q: 0, r: 0, s: 0 };

  // remember whether the leftmost column is "odd-q" or "even-q"
  // https://www.redblobgames.com/grids/hexagons/#coordinates
  let oddQ = true;
  for (let x = 0; x < stepsLeft; x++) {
    oddQ = x % 2 === 0;
    topLeftHexCoordinates = hexStep(
      topLeftHexCoordinates,
      oddQ ? HexDirection.UP_LEFT : HexDirection.DOWN_LEFT,
    );
  }
  for (let y = 0; y < stepsUp; y++) {
    topLeftHexCoordinates = hexStep(topLeftHexCoordinates, HexDirection.UP);
  }

  let minElevation = 1;
  let maxElevation = 0;

  // multipliers for the different noise results:
  // elevation noise factor - based on the user-defined jaggedness param
  const elvNFac = 7.2 * (generatorState.jaggedness ** 3) + 0.1;
  // woodiness noise offset (fixed for now)
  const wdOffset = { x: 123456, y: -654321 };
  // woodiness noise factor (fixed for now)
  const wdNFac = 4;
  // humidity noise offset (fixed for now)
  const hmdOffset = { x: -654321, y: 123456 };
  // humidity noise factor (fixed for now)
  const hmdNFac = 4;
  // roughness noise offset (fixed for now)
  const rghOffset = { x: -123456, y: 654321 };
  // roughness noise factor (fixed for now)
  const rghNFac = 5;

  // go column by column, adding the hexes from top to bottom
  let columnTopHexCoordinates = { ...topLeftHexCoordinates };
  for (let column = 0; column < generatorState.mapSize.width; column++) {
    let currentHexCoordinates = { ...columnTopHexCoordinates };
    for (let row = 0; row < generatorState.mapSize.height; row++) {
      const cartesianCoordinates = hexCoordinatesToPoint(currentHexCoordinates);
      const mapSizeFactor = mapWidth < mapHeight ? mapWidth : mapHeight;
      // const mapSizeFactor = (mapWidth + mapHeight) * 0.5;
      const nx = cartesianCoordinates.x / mapSizeFactor - 0.5;
      const ny = cartesianCoordinates.y / mapSizeFactor - 0.5;

      // get separate noise results for elevation, woodiness and humidity
      const elevation = scaleNoise(generatorState.noiseFn(elvNFac * nx, elvNFac * ny));
      const woodiness = scaleNoise(generatorState.noiseFn(wdNFac * nx + wdOffset.x, wdNFac * ny + wdOffset.y));
      const humidity = scaleNoise(generatorState.noiseFn(hmdNFac * nx + hmdOffset.x, hmdNFac * ny + hmdOffset.y));
      const roughness = scaleNoise(generatorState.noiseFn(rghNFac * nx + rghOffset.x, rghNFac * ny + rghOffset.y));
      minElevation = Math.min(minElevation, elevation);
      maxElevation = Math.max(maxElevation, elevation);
      const field: HexField = {
        ...currentHexCoordinates,
        key: getHexFieldKey(currentHexCoordinates),
        size: generatorState.hexSize,
        passable: true,
        elevation,
        heightLevel: 0, // will be set later, based on the elevation and user-provided params
        type: HexFieldType.GRASS, // will be modified later, based on multiple parameters
        woodiness,
        humidity,
        totalHumidity: humidity, // will be modified later, based on user params and neighboring water hexes
        roughness,
      };
      fields[field.key] = field;
      currentHexCoordinates = hexStep(currentHexCoordinates, HexDirection.DOWN);
    }
    // set next column's top hex coordinates
    const columnOdd = oddQ ? column % 2 === 0 : column % 2 === 1;
    columnTopHexCoordinates = hexStep(
      columnTopHexCoordinates,
      columnOdd ? HexDirection.DOWN_RIGHT : HexDirection.UP_RIGHT,
    );
  }

  const topLeftCartesianCoordinates = hexCoordinatesToPoint(topLeftHexCoordinates);
  const top = topLeftCartesianCoordinates.y - spacing.v * (oddQ ? 0.5 : 1);
  const rect: Rectangle2d = {
    left: topLeftCartesianCoordinates.x - generatorState.hexSize,
    top,
    right: topLeftCartesianCoordinates.x - generatorState.hexSize + mapWidth,
    bottom: top + mapHeight,
  };

  generatorState.generatedMap.hexFields = fields;
  generatorState.generatedMap.rect = rect;
  generatorState.generatedMap.mapDimensionsInGameUnits = {
    width: mapWidth,
    height: mapHeight,
  };
  generatorState.generatedMap.minElevation = minElevation;
  generatorState.generatedMap.maxElevation = maxElevation;
  generatorState.generatedMap.isOddQ = oddQ;
}

/**
 * Helper function that scales noise to the 0 - 1 range
 * @param n the noise result
 */
function scaleNoise(n: number) {
  return (n + 1) * 0.5;
}
