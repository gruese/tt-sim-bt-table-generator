import type { HexCoordinates } from '@/modules/common/types';
import { KEY_SEPARATOR } from '@/modules/common/constants';

export function getHexFieldKey(hex: HexCoordinates) {
  return hex.q + KEY_SEPARATOR + hex.r + KEY_SEPARATOR + hex.s;
}
