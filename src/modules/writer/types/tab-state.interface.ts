import type { RgbColor } from '.';

export interface TabState {
  title: string;
  body: string;
  color: string;
  visibleColor: RgbColor;
  id: number;
}
