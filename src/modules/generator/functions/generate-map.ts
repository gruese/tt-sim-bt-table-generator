import { initializeMap } from '@/modules/generator/functions/initialize-map';
import { applyParameters } from '@/modules/generator/functions/apply-parameters';

export function generateMap() {
  initializeMap();
  applyParameters();
}
