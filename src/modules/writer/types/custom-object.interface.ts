import type { RgbColor, RgbaColor } from '.';

export interface CustomObject {
  Nickname: string;
  ColorDiffuse?: RgbColor | RgbaColor;
  CustomMesh: {
    MeshURL: string;
    DiffuseURL: string;
    NormalURL: string;
    ColliderURL: string;
    Convex: boolean;
    MaterialIndex: number;
    TypeIndex: number;
    CustomShader: {
      SpecularColor: RgbColor | RgbaColor;
      SpecularIntensity: number;
      SpecularSharpness: number;
      FresnelStrength: number;
    };
    CastShadows: boolean;
  };
}
