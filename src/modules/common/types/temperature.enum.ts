export enum Temperature {
  'FREEZING' = -2,
  'COLD' = -1,
  'MILD' = 0,
  'HOT' = 1,
  'SCORCHING' = 2,
}
