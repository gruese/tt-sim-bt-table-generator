import { describe, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';
import HomePage from './home-page.vue';

describe('Home Page', () => {
  it('renders without props', () => {
    const wrapper = mount(HomePage);
    expect(wrapper.text()).toContain('Sic Semper Terrainis')
  });
});
