export function temperatureFormatter(v: number, lowercase = false) {
  let r= '';
  switch (v) {
    case -2:
      r = 'Frozen';
      break;
    case -1:
      r = 'Cold';
      break;
    case 0:
      r = 'Mild';
      break;
    case 1:
      r = 'Hot';
      break;
    case 2:
      r = 'Scorching';
      break;
    default:
      return '?';
  }
  return lowercase ? r.toLowerCase() : r;
}
