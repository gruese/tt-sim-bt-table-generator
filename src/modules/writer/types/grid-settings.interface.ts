import type { RgbColor } from '.';

export interface GridSettings {
  Type: number;
  Lines: boolean;
  Color: RgbColor;
  Opacity: number;
  ThickLines: boolean;
  Snapping: boolean;
  Offset: boolean;
  BothSnapping: boolean;
  xSize: number;
  ySize: number;
  PosOffset: {
    x: number;
    y: number;
    z: number;
  };
}
