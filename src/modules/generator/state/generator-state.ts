import { computed, reactive } from 'vue';
import type { GeneratorState } from '../types';
import { MAP_SIZE_DIMENSIONS } from '@/modules/generator/constants';
import { createFractalNoiseFn } from '@/modules/common/functions/noise/create-fractal-noise-fn';
import { Temperature, type HexField } from '@/modules/common/types';

const DEFAULT_NOISE = (x: number, y: number) => x + y;

export const generatorState = reactive<GeneratorState>({
  seed: '',
  hexSize: 1,
  changesPending: true,
  mapSize: {
    width: MAP_SIZE_DIMENSIONS.long,
    height: MAP_SIZE_DIMENSIONS.short,
  },
  noiseFn: DEFAULT_NOISE,
  numberOfNoiseOctaves: 6,
  maxWaterDepth: 2,
  maxHeightLevel: 5,
  lunar: false,
  jaggedness: 0.5,
  verticality: 0.5,
  waterCover: 0.175,
  humidity: 0.5,
  humiditySpread: 0.15,
  temperature: Temperature.MILD,
  treeCover: 0.3,
  heavyWoods: 0.15,
  roughGrounds: 0.1,
  generatedMap: {
    hexFields: {},
    hexFieldsMods: {},
    mapDimensionsInGameUnits: {
      width: 0,
      height: 0,
    },
    rect: {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
    },
    maxElevation: 0,
    minElevation: 0,
    isOddQ: false,
  },
});

export const combinedFields = computed(() => {
  const ret: Record<string, HexField> = {};
  Object.keys(generatorState.generatedMap.hexFields).forEach((hexKey) => {
    ret[hexKey] = {
      ...generatorState.generatedMap.hexFields[hexKey],
      ...(generatorState.generatedMap.hexFieldsMods[hexKey] || {}),
    };
  });
  return ret;
});

export function randomizeSeed(length = 20) {
  const alphabet = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let seed = '';
  for (let i = 0; i < length; i++) {
    seed += alphabet[Math.floor(Math.random() * alphabet.length)];
  }
  generatorState.seed = seed;
}

/**
 * Completely resets the generation process, removes the noise function and all generated field data.
 */
export function resetSeed() {
  generatorState.seed = '';
  generatorState.noiseFn = DEFAULT_NOISE;
  generatorState.generatedMap.hexFields = {};
}

/**
 * Generates fractal noise based on the seed and number of octaves provided.
 */
export function updateNoiseFn() {
  generatorState.noiseFn = createFractalNoiseFn(generatorState.numberOfNoiseOctaves, generatorState.seed);
}

/**
 * Adds a manual modification to a hex field (by id - does not have to be currently visible)
 *
 * @param hexKey The hex's key
 * @param mod The modification, may change any attribute of a HexField
 */
export function addHexFieldMod(hexKey: string, mod: Partial<HexField>) {
  if (mod.type === undefined) {
    delete mod.type;
  }
  generatorState.generatedMap.hexFieldsMods[hexKey] = {
    ...(generatorState.generatedMap.hexFieldsMods[hexKey] || {}),
    ...mod,
  }
}

/**
 * Removes any manual hex modifications on a given hex
 *
 * @param hexKey The hex's key
 */
export function removeHexFieldMod(hexKey: string) {
  delete generatorState.generatedMap.hexFieldsMods[hexKey];
}

/**
 * Returns true if a hex field has modifications applied.
 *
 * @param hexKey The hex key
 */
export function hexFieldHasMods(hexKey: string): boolean {
  return Object.keys(generatorState.generatedMap.hexFieldsMods[hexKey] || {}).length > 0;
}

/**
 * Persists the current settings and map to local storage so that the map can be easily loaded and
 * regenerated.
 */
export function saveGeneratorStateToLocalStorage() {
  const saveObj = {
    ...generatorState,
    noiseFn: undefined,
  };
  delete saveObj['noiseFn'];
  localStorage.setItem('generatorState', JSON.stringify(saveObj));
  console.log('Saved generator state to browser storage');
}

/**
 * Loads the current settings and map from local storage.
 */
export function loadGeneratorStateFromLocalStorage() {
  try {
    const possibleSavedStateString = localStorage.getItem('generatorState');
    if (!possibleSavedStateString) {
      console.warn('Cannot restore: No saved generator state found in browser storage');
      return;
    }
    const savedState = JSON.parse(possibleSavedStateString);
    Object.keys(generatorState).forEach((key) => {
      if (savedState.hasOwnProperty(key)) {
        (generatorState as any)[key as keyof GeneratorState] = savedState[key];
      }
    });
    console.log('Restored generator state from browser storage');
  } catch (e) {
    console.error(e);
  }
}

// run once when initially executing this code
randomizeSeed();
