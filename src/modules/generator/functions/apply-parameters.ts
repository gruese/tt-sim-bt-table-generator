import { generatorState } from '@/modules/generator/state';
import {
  clampNumber,
  determineMeanAndSigma,
  determineProbabilityValueLimit,
  getHexFieldKey,
  hexStep,
} from '@/modules/common/functions';
import {
  HexDirection,
  type HexField,
  HexFieldType,
  RoughnessType,
  Temperature,
  WoodsType
} from '@/modules/common/types';

/**
 * When a map has been initialized, this function applies all the different parameters.
 */
export function applyParameters() {
  const fieldList: Array<HexField> = Object.values(generatorState.generatedMap.hexFields);
  if (fieldList.length === 0) {
    return;
  }
  const { mean: elevationMean, sigma: elevationSigma } = determineMeanAndSigma(fieldList.map((field) => field.elevation));
  const waterLine = clampNumber(
    determineProbabilityValueLimit(
      elevationMean,
      elevationSigma,
      Math.round(generatorState.waterCover * 100)
    ),
    0,
    1,
  );
  const { mean: woodMean, sigma: woodSigma } = determineMeanAndSigma(
    fieldList
      .filter((field) => field.elevation >= waterLine)
      .map((field) => field.woodiness),
  );
  const lightWoodsLimit = determineProbabilityValueLimit(
    woodMean,
    woodSigma,
    Math.round(generatorState.treeCover * 100),
  );
  const heavyWoodsLimit = determineProbabilityValueLimit(
    woodMean,
    woodSigma,
    Math.round(generatorState.treeCover * generatorState.heavyWoods * 100),
  );

  const { mean: roughnessMean, sigma: roughnessSigma } = determineMeanAndSigma(
    fieldList
      .filter((field) => field.elevation >= waterLine)
      .map((field) => field.roughness),
  );
  const roughGroundsLimit = determineProbabilityValueLimit(
    roughnessMean,
    roughnessSigma,
    Math.round(generatorState.roughGrounds * 100),
  );

  // calculate elevation level floors for land hexes
  const landHeightLevelFloors: Array<number> = calculateLevelFloors(
    [],
    {
      min: waterLine,
      max: generatorState.generatedMap.maxElevation
    },
    generatorState.maxHeightLevel + 1,
    generatorState.verticality,
  );
  // calculate elevation level floors for water hexes
  const waterHeightLevelFloors: Array<number> = calculateLevelFloors(
    [],
    {
      min: generatorState.generatedMap.minElevation,
      max: waterLine,
    },
    generatorState.maxWaterDepth,
    1 - generatorState.verticality, // reverse weighting, as water depth level increases as elevation drops
  );

  // arid / grass / swamp: dependent on humidity
  // Probabilty of a hex being arid: -(humidity-1)^3
  // Probability of a hex being swampy: 7.8*(humidity-0.5)^3 + 0.025
  const { mean: humidityMean, sigma: humiditySigma } = determineMeanAndSigma(
    fieldList.map((field) => field.humidity),
  );
  const aridLimit = determineProbabilityValueLimit(
    humidityMean,
    humiditySigma,
    Math.round(100 * -((generatorState.humidity - 1) ** 3)),
  );
  const grassLimit = determineProbabilityValueLimit(
    humidityMean,
    humiditySigma,
    Math.round(100 * (1 - (7.8 * ((generatorState.humidity - 0.5) ** 3) + 0.025))),
  );
  let waterHexes = 0;

  // console.log('water levels', waterHeightLevelFloors, generatorState.generatedMap.minElevation, waterLine);
  // 1st pass: Identify water hexes and propagate humidity from them
  fieldList.forEach((field) => {
    if (field.elevation < waterLine) {
      // water hex
      field.type = generatorState.temperature === Temperature.FREEZING
        ? HexFieldType.ICE_ON_WATER
        : generatorState.temperature === Temperature.SCORCHING
        ? HexFieldType.LAVA
        : HexFieldType.WATER;
      // hex height level
      for (let i = 0; i < generatorState.maxWaterDepth; i++) {
        if (i >= waterHeightLevelFloors.length - 1 || field.elevation <= waterHeightLevelFloors[i + 1]) {
          field.heightLevel = -(generatorState.maxWaterDepth - i);
          break;
        }
      }
      waterHexes++;
      // propagate a small amount of humidity to the surrounding hexes
      for (let dir = 0; dir < 6; dir++) {
        const neighborKey = getHexFieldKey(hexStep(field, dir as HexDirection));
        const neighbor = generatorState.generatedMap.hexFields[neighborKey];
        if (neighbor) {
          neighbor.totalHumidity += generatorState.humiditySpread;
        }
      }
    }
  });

  // 2nd pass: Propagate humidity from non-water hexes
  // fieldList
  //   .filter((field) => field.type !== HexFieldType.WATER && field.totalHumidity > grassLimit)
  //   .forEach((field) => {
  //     // propagate a small amount of humidity to the surrounding hexes
  //   },
  // );

  // 3rd pass: Finalize the land hex fields
  fieldList.filter((field) => ![
    HexFieldType.WATER,
    HexFieldType.LAVA,
    HexFieldType.ICE_ON_WATER
  ].includes(field.type)).forEach((field) => {
    if (generatorState.lunar) {
      field.type = HexFieldType.LUNAR;

    // humidity (arid / grass / swamp)
    } else if (field.totalHumidity < aridLimit) {
      // low humidity
      switch (generatorState.temperature) {
        case Temperature.FREEZING:
        case Temperature.COLD:
          field.type = HexFieldType.TUNDRA;
          break;
        case Temperature.MILD:
          field.type = HexFieldType.DESERT;
          break;
        case Temperature.HOT:
          field.type = HexFieldType.SAND;
          break;
        case Temperature.SCORCHING:
          field.type = HexFieldType.VOLCANIC;
      }
    } else if (field.totalHumidity >= grassLimit) {
      // high humidity
      switch (generatorState.temperature) {
        case Temperature.FREEZING:
          field.type = HexFieldType.ICE;
          break;
        case Temperature.COLD:
          field.type = HexFieldType.MARSH;
          break;
        case Temperature.MILD:
          field.type = HexFieldType.SWAMP;
          break;
        case Temperature.HOT:
          field.type = HexFieldType.BOG;
          break;
        case Temperature.SCORCHING:
          field.type = HexFieldType.GEYSERS;
      }
    } else {
      // medium humidity
      switch (generatorState.temperature) {
        case Temperature.FREEZING:
          field.type = HexFieldType.ICE;
          break;
        case Temperature.COLD:
          field.type = HexFieldType.SNOW;
          break;
        case Temperature.MILD:
          field.type = HexFieldType.GRASS;
          break;
        case Temperature.HOT:
          field.type = HexFieldType.WASTELAND;
          break;
        case Temperature.SCORCHING:
          field.type = HexFieldType.ASH;
      }
    }
    // woods
    // TODO: Jungle (humid + forested)
    if (field.woodiness < heavyWoodsLimit) {
      field.woodsType = field.type === HexFieldType.SWAMP
        ? WoodsType.JUNGLE_HEAVY
        : field.type === HexFieldType.DESERT
          ? WoodsType.DRY_HEAVY
          : WoodsType.HEAVY;
    } else if (field.woodiness < lightWoodsLimit) {
      field.woodsType = field.type === HexFieldType.SWAMP
        ? WoodsType.JUNGLE_LIGHT
        : field.type === HexFieldType.DESERT
          ? WoodsType.DRY_LIGHT
          : WoodsType.LIGHT;
    }
    if (field.type === HexFieldType.SWAMP && field.woodsType) {
      field.type = HexFieldType.GRASS;
    }
    // rough ground (only possible for non-wood land hexes)
    if (!field.woodsType && field.roughness < roughGroundsLimit) {
      field.roughType = RoughnessType.ROUGH;
    }
    // hex height level
    for (let i = 0; i <= generatorState.maxHeightLevel; i++)  {
      if (i >= landHeightLevelFloors.length - 1 || field.elevation < landHeightLevelFloors[i + 1]) {
        field.heightLevel = i;
        break;
      }
    }
  });
  console.log(`${waterHexes}/${fieldList.length} water hexes`);
}

function calculateLevelFloors(levelFloors: Array<number>, range: { min: number, max: number }, numberOfLevels = 1, verticality = 0.5) {
  if (numberOfLevels <= 1) {
    levelFloors.unshift(range.min);
    return levelFloors;
  }
  const factor = 1 / (1 + (2 * (numberOfLevels - 1) * (1 - verticality)));
  const highestLevelFloor = range.max - (range.max - range.min) * factor;
  levelFloors.unshift(highestLevelFloor);
  return calculateLevelFloors(levelFloors, { min: range.min, max: highestLevelFloor }, numberOfLevels - 1, verticality);
}
