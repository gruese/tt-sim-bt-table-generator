export enum GeneratorStep {
  'SEED' = 'SEED',
  'PARAMETERS' = 'PARAMETERS',
  'MANUAL' = 'MANUAL',
}
