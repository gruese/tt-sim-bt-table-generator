import { STANDARD_NORMAL_DISTRIBUTION_TABLE } from '@/modules/common/constants';

/**
 * Use mean and standard deviation of a normal distribution and return the value X
 * below which a given percentage the values lie => P(x < X) based on the values
 * in the standard normal table https://en.wikipedia.org/wiki/Standard_normal_table.
 *
 * @param mean The mean of the distribution
 * @param sigma The standard deviation of the distribution
 * @param percentage The percentage of values that should be below X, as an integer (--> 1 = 1%)
 */
export function determineProbabilityValueLimit(mean: number, sigma: number, percentage: number) {
  // use look-up table to find Z for the percentage
  const bigZ = STANDARD_NORMAL_DISTRIBUTION_TABLE[Math.min(100, Math.max(0, percentage))];
  // calculate X from Z
  return bigZ * sigma + mean;
}
