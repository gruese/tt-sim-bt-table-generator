import type { HexCoordinates } from '@/modules/common/types';
import { HexDirection } from '@/modules/common/types';

export function hexStep(hex: HexCoordinates, direction: HexDirection): HexCoordinates {
  switch (direction) {
    case HexDirection.UP:
      return {
        q: hex.q,
        r: hex.r - 1,
        s: hex.s + 1,
      };
    case HexDirection.UP_RIGHT:
      return {
        q: hex.q + 1,
        r: hex.r - 1,
        s: hex.s,
      };
    case HexDirection.DOWN_RIGHT:
      return {
        q: hex.q + 1,
        r: hex.r,
        s: hex.s - 1,
      };
    case HexDirection.DOWN:
      return {
        q: hex.q,
        r: hex.r + 1,
        s: hex.s - 1,
      };
    case HexDirection.DOWN_LEFT:
      return {
        q: hex.q - 1,
        r: hex.r + 1,
        s: hex.s,
      };
    case HexDirection.UP_LEFT:
      return {
        q: hex.q - 1,
        r: hex.r,
        s: hex.s + 1,
      };
  }
}
