import type { GridSettings, LightingSettings, ObjectState, TabState } from '.';

export interface SaveGame {
  SaveName: string;
  EpochTime: number;
  Date: string;
  VersionNumber: string;
  GameMode: string;
  Tags: Array<string>;
  Gravity: number;
  PlayArea: number;
  Table: number;
  Sky: number;
  Note: number;
  TabStates: Record<string, TabState>;
  Grid: GridSettings;
  Lighting: LightingSettings;
  Hands: {
    Enable: boolean;
    DisableUnused: boolean;
    Hiding: number;
  };
  ComponentTags: {
    labels: Array<string>;
  };
  Turns: {
    Enable: boolean;
    Type: number;
    TurnOrder: Array<string>;
    Reverse: boolean;
    SkipEmpty: boolean;
    DisableInteractions: boolean;
    PassTurns: boolean;
  };
  DecalPallet: Array<string>;
  LuaScript: string;
  LuaScriptState: string;
  XmlUI: string;
  ObjectStates: Array<ObjectState>;
}