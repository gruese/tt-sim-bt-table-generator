
import { makeZeroPositive } from '@/modules/common/functions';
import type { HexCoordinates } from '@/modules/common/types';

/**
 * Rounds a fractional coordinate to the closest actual hex coordinate.
 * @see https://www.redblobgames.com/grids/hexagons/#rounding
 * @param hex The hex coordinate to round
 * @returns Rounded hex coordinates (a new object)
 */
export function hexCoordinateRound(hex: HexCoordinates) {
  const rounded = {
    q: Math.round(hex.q),
    r: Math.round(hex.r),
    s: Math.round(hex.s),
  };
  const diff = {
    q: Math.abs(rounded.q - hex.q),
    r: Math.abs(rounded.r - hex.r),
    s: Math.abs(rounded.s - hex.s),
  };

  if (diff.q > diff.r && diff.q > diff.s) {
    rounded.q = -rounded.r - rounded.s;
  } else if (diff.r > diff.s) {
    rounded.r = -rounded.q - rounded.s;
  } else {
    rounded.s = -rounded.q - rounded.r;
  }
  rounded.q = makeZeroPositive(rounded.q);
  rounded.r = makeZeroPositive(rounded.r);
  rounded.s = makeZeroPositive(rounded.s);
  return rounded;
}
