import { type Dimensions2d, type HexField, type Rectangle2d, type Temperature } from '@/modules/common/types';

export interface GeneratorState {
  seed: string; // the map seed
  hexSize: number; // size (= horizontal "radius") of each hex (normally 1)
  changesPending: boolean; // unapplied changes pending
  mapSize: Dimensions2d; // map size in hexes (horizontal / vertical)
  numberOfNoiseOctaves: number; // number of octaves for the fractal noise function
  noiseFn: (x: number, y: number) => number; // the seeded noise function
  generatedMap: {
    hexFields: Record<string, HexField>; // the generated hex fields
    hexFieldsMods: Record<string, Partial<HexField>>; // the manually set hex field changes
    mapDimensionsInGameUnits: Dimensions2d; // map size in game units
    rect: Rectangle2d; // map bounds in game units
    minElevation: number; // minimum elevation in the generated samples
    maxElevation: number; // maximum elevation in the generated samples
    isOddQ: boolean; // whether the top left hex is "odd-q" or "even-q"
  }
  lunar: boolean; // true if surface is lunar (most other parameters are ignored
  jaggedness: number; // few large vs many small features
  verticality: number; // amount of high ground / height level differences
  waterCover: number; // percentage of water hexes
  maxHeightLevel: number;
  maxWaterDepth: number;
  humidity: number; // humidity level
  temperature: Temperature; // temperature
  treeCover: number; // percentage of tree hexes
  heavyWoods: number; // percentage of heavy woods in relation to all wood hexes
  roughGrounds: number; // percentage of hexes with rough terrain
  humiditySpread: number; // fraction of surplus humidity spread to surrounding hexes)
}
